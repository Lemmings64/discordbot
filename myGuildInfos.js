module.exports = class myGuild {
	constructor(discord) {
		this.discord = discord;
		this.emojiRed = '557359772895019008';
		this.emojiBlue = '557360498547490836';
		this.emojiYellow = '557360499910639617';
		this.userLemmings = '<@300590271459098629>';
		this.roleBot = '585441901101056011';
		this.roleAdmin = '789577654893543505';
		this.IdontRememberWhatItIs = '390528937618571266';
	}
	get guild() {
		return this.discord.guilds.cache.get('381021639359463424');
	}
	
	get channelTest() {
		return this.discord.channels.cache.get('638359902628347907');
	}
	get channelRaids() {
		return this.discord.channels.cache.get('585042571722489856');
	}
	get channelBla() {
		return this.discord.channels.cache.get('381021639812317187');
	}
	get channelEvent() {
		return this.discord.channels.cache.get('745752415588057228');
	}
	get channelSeek() {
		return this.discord.channels.cache.get('706862570455433226');
	}
	get channelLog() {
		return this.discord.channels.cache.get('769998902426075136');
	}
	
	get roleCasu() {
		return this.guild.roles.cache.get('585397799236141056');
	}
	get roleValor() {
		return this.guild.roles.cache.get('453958164426129408');
	}
	get roleMystic() {
		return this.guild.roles.cache.get('453957002721230878');
	}
	get roleInstinct() {
		return this.guild.roles.cache.get('453956436226080781');
	}
};

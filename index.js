const Discord = require('discord.js');
const moment = require('moment');
const http = require('http');
const https = require('https');
const Parser = require('rss-parser');
const qs = require('querystring');
const fs = require('fs');
const _ = require('lodash');
const cron = require('node-cron');
const MyGuild = require('./myGuildInfos.js');
const Twit = require('./twitter_events/twitterEvents.js');
const TvShows = require('./tv_shows/tvShows.js');
const Pogo = require('./pokemon/pokemon.js');
const auth = require('./auth.json');
//npm install discord.js winston moment http https rss-parser querystring lodash node-cron
var posted = [];
var myGuild = {};
var twit = {};
var tvShows = {};
var pogo = {};

const bot = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

bot.on('ready', function (evt) {
    myGuild = new MyGuild(bot);
	twit = new Twit(bot, myGuild, sendMsgAsync);
	tvShows = new TvShows(bot, myGuild, sendMsgAsync);
	pogo = new Pogo(bot, myGuild, sendMsgAsync, sendAsync);
	doAction({content:'!start'}, true);
	log('starting');
});

bot.on('message', (message) => {
	//const isBot = message.author.bot;
	myGuild.guild.members.fetch(message.author.id).then(member => {
		const isAdmin = message.member.hasPermission('ADMINISTRATOR');
		doAction(message, isAdmin);
		//const hasRoleAdmin = member.roles.cache.some(r => r.id === myGuild.roleAdmin);
	}).catch(log);
	
});

function doAction(message, isAdmin) {
	if (message.content === '!help') {
		message.reply(help());
	}
	if (_.startsWith(message.content, '!pogo ')) {
		pogo.doAction(message, isAdmin);
    }
	if (_.startsWith(message.content, '!twit ')) {
		twit.doAction(message, isAdmin);
    }
	if (_.startsWith(message.content, '!tv ')) {
		tvShows.doAction(message, isAdmin);
    }
    if (message.content === '!ping') {
		message.reply('pong');
    }
	if(!isAdmin){return;}
    if (message.content === '!start') {
		pogo.doAction({content:'!pogo start'}, true);
		twit.doAction({content:'!twit start'}, true);
		tvShows.doAction({content:'!tv start'}, true);
    }
	if (message.content === '!stop') {
		pogo.doAction({content:'!pogo stop'}, true);
		twit.doAction({content:'!twit stop'}, true);
		tvShows.doAction({content:'!tv stop'}, true);
		setTimeout(() => bot.destroy(), 2000);
	}
	if (message.content === '!del') {
		del(message);
    }
}

function del(message) {
	if (message.channel.type == 'text') {
		message.channel.messages.fetch().then(messages => {
			const botMessages = messages.filter(msg => msg.author.bot || _.startsWith(msg.content, '!'));
			message.channel.bulkDelete(botMessages).catch(log);
		}).catch(log);
	}
}

bot.on("guildMemberAdd", (member) => {
	// I think this part is not working anymore. It needs to be rework with DiscordJS V12
	let canal = bot.channels.cache.get('381021639812317187'); //bla bla
	canal.send('Bonjour, '+ member.user.username +' Bienvenue dans le coin du casu ! :)').catch(log);
	canal.send('Choose your color wisely !').then((msg) => {
		msg.react(myGuild.emojiRed)
		.then(() => msg.react(myGuild.emojiBlue))
		.then(() => msg.react(myGuild.emojiYellow))
		.catch(log);
	});
});

bot.on('messageReactionAdd', (reaction, user) => {
	(reaction.partial ? reaction.fetch() : Promise.resolve(reaction)).then(reaction => {
		if(reaction.message.author.username==='meudonSeineBot'){
			myGuild.guild.members.fetch(user).then(member => {
				if(reaction.emoji.name === 'spinda') {
						member.roles.add(myGuild.roleCasu);
				} else if(reaction.emoji.name === 'Psychic') {
						member.roles.remove(myGuild.roleCasu);
				} else if(reaction.emoji.name === 'valor') {
						member.roles.add(myGuild.roleValor);
						member.roles.remove(myGuild.roleMystic);
						member.roles.remove(myGuild.roleInstinct);
				} else if(reaction.emoji.name === 'mystic') {
						member.roles.add(myGuild.roleMystic);
						member.roles.remove(myGuild.roleValor);
						member.roles.remove(myGuild.roleInstinct);
				} else if(reaction.emoji.name === 'instinct') {
						member.roles.add(myGuild.roleInstinct);
						member.roles.remove(myGuild.roleValor);
						member.roles.remove(myGuild.roleMystic);
				}
			});
		}
	}).catch(log);
});

function help(){
	return `BOT HELP : 
\`\`\`
!pogo help      : (everyone)      show bot command for pokemon go
!twit help      : (everyone)      show bot command for twitter PoGo and HPWU event alert
!tv help        : (everyone)      show bot command for tv shows alert
!ping           : (everyone)      reply pong if bot is online
!start          : (admin)         => !pogo start + !twit start + !tv start
!stop           : (admin)         => !pogo stop + !twit stop + !tv stop + stop the bot
!del            : (admin)         delete all recent messages from the bot or for him (!command)
\`\`\`
`;
}

async function sendAsync (canal, msg) {
	await canal.send(msg).then(msg => msg.delete({ timeout: 7200000 })).catch('Error while posting/deleting raid');
}

async function sendMsgAsync (canal, msg) {
	await canal.send(msg).catch(log);
}
function sendMsg (canal, msg) {
	canal.send(msg).catch(log);
}

function log (error) {
	try{
		error = JSON.stringify(error);
	}catch{
		error = "error on the error";
	}
	myGuild.channelLog.send(error).catch(console.error);
}
bot.login(auth.token);

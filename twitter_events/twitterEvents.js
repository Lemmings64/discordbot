const Discord = require('discord.js');
const moment = require('moment');
const express = require('express');
const bodyParser = require('body-parser');
const util = require('util');
const request = require('request');
const path = require('path');
const socketIo = require('socket.io');
const http = require('http');
const cron = require('node-cron');
const oAuth = require('./auth_t.json');
const token = oAuth;
const post = util.promisify(request.post);
const get = util.promisify(request.get);
//npm install discord.js moment express body-parser util request path socket.io http node-cron
const rulesURL = new URL('https://api.twitter.com/2/tweets/search/stream/rules');
const streamURL = new URL('https://api.twitter.com/2/tweets/search/stream?tweet.fields=entities&expansions=attachments.media_keys&media.fields=url');
const getThread = 'https://api.twitter.com/2/tweets/search/recent?query=conversation_id:';
const openTweet = 'https://api.twitter.com/2/tweets/TWEET_ID?tweet.fields=entities&expansions=attachments.media_keys&media.fields=url';
const hpwuEventTags = ["wizardingweekend","brilliantevent","communityday"];
const hpwuTags = ["hpwu","hpwizardsunite","wizardsunite"];

module.exports = class twit {
	constructor(discord, myGuild, send, log) {
		this.discord = discord;
		this.myGuild = myGuild;
		this.channelLog = this.myGuild.channelLog;
		this.send = send;
		this.log = log;
		this.hpwuEventTags = hpwuEventTags;
		this.hpwuTags = hpwuTags;
		this.timeout = 0;
		this.lastHeartbeat = null;
		this.hadHeartbeatError = false;
		this.stream = null;
		this.streamCron = cron.schedule('* * * * *', () => this.checkHeartbeat(), {scheduled: false});
		this.state = null;
	}

	streamTweets(){
		const config = { url: streamURL, auth: { bearer: token }, timeout: 31000 };
		try {
			if(this.stream){
				this.stream.abort();
			}
			this.stream = request.get(config);
			this.stream
				.on('data', (data) => {
					try {
						const json = JSON.parse(data);
						if (!json.connection_issue) {
							if (json.data) {
								// Let the streamOnData finish before to work with the data
								Promise.resolve(json).then(json => this.useTweet(json))
							} else {
								this.updateState('twit stream', 'no data received', json);
							}
						} else {
							this.updateState('twit stream', 'connection_issue', json);
							this.reconnect();
						}
					} catch (e) {
						this.lastHeartbeat = moment();
						this.updateState('twit stream', 'heartbeat', null, 'info'); 
					}
				})
				.on('error', (err) => { // Connection timed out
					this.updateState('twit stream', 'crashed', err);
					this.reconnect();
				});
		} catch (err) {
			this.updateState('twit stream', 'authenticate', err);
		}
	}
	
	useTweet(json){
		if(json.matching_rules[0].tag === 'test'){
			if(json.data.entities && json.data.entities.hashtags){
				let eventFound = null, game = '';
				json.data.entities.hashtags.forEach(hash=>{
					game = game || this.hpwuTags.includes(hash.tag) ? 'hpwu' : 'pogo';
					eventFound = eventFound || this.hpwuEventTags.includes(hash.tag);
				});
				if(eventFound && game === 'hpwu'){
					this.postPic(json, this.myGuild.channelEvent);
					this.openThread(json.data.id);
				}
			}
		}else if(json.matching_rules[0].tag === 'debug'){
			this.send (this.channelLog, JSON.stringify(json.data));
		}	
	}
	
	async openThread(threadId){
		const getThreadURL = new URL(getThread+threadId);
		const requestConfig = { url: getThreadURL, auth: { bearer: token }, json: true };
		try {
			const response = await get(requestConfig);
			if (response.statusCode === 200) {
				if(response.body.data[0]){
					this.getNextPic(response.body.data[0].id);
				}
				this.log('twit open thread', 'will try to get next pic', threadId, 'success');
			}else if (response.statusCode === 403) {
				this.log('twit open thread', '403', response.body);
			} else {
				this.log('twit open thread', '!200', response);
			}
		} catch (err) {
			this.log('twit open thread', 'getting', err);
		} 
	}
	
	async getNextPic(id){
		const openTweetURL = new URL(openTweet.replace('TWEET_ID',id));
		const requestConfig = { url: openTweetURL, auth: { bearer: token }, json: true };
		try {
			const response = await get(requestConfig);
			if (response.statusCode === 200) {
				this.postPic(response.body, this.myGuild.channelEvent);
				this.log('twit get next pic', 'will try to post pic', id, 'success');
			}else if (response.statusCode === 403) {
				this.log('twit get next pic', '403', response.body);
			} else {
				this.log('twit get next pic', '!200', response);
			}
		} catch (err) {
			this.log('twit get next pic', 'getting', err);
		}
	}
	
	postPic(json, channel){
		if(json.includes && json.includes.media){
			let imgs = json.includes.media.map(x=>x.url);
			this.send (channel, {files:imgs});
		}else{
			this.log('twit post pic', 'no pic found', json);
		}
	}

	async reconnect() {
		this.timeout++;
		this.stream.abort();
		await this.sleep(2 ** this.timeout * 1000);
		this.streamTweets();
	}
	
	async start() {
		const embed = new Discord.MessageEmbed()
			.setColor('GREEN')
			.setTitle('TWIT')
			.setDescription('started')
			.setTimestamp()
		if(this.state){
			await this.state.edit(embed);
		}else{
			await this.myGuild.channelStates.send(embed).then(msg => {this.state = msg});
		}
		if(this.stream){
			this.stream.abort();
		}
		await this.sleep(9000);
		this.streamTweets();
		this.streamCron.start();
	}
	
	stop() {
		if(this.stream){
			this.stream.abort();
		}
		this.streamCron.destroy();
		this.myGuild.channelStates.bulkDelete([this.state]);
		this.state = null;
	}

	async sleep (delay) {
		return new Promise((resolve) => setTimeout(() => resolve(true), delay));
	}

	async addRules(rules){
		const requestConfig = { url: rulesURL, auth: { bearer: token }, json: rules };
		try {
			const response = await post(requestConfig);
			if (response.statusCode === 200 || response.statusCode === 201) {
				this.log('twit add rules', 'posted', response.body.data, 'success');
			} else {
				this.log('twit add rules', '!200', response);
			}
		} catch (err) {
			this.log('twit add rules', 'posting', err);
		}
	}
	
	async getRules(){
		const requestConfig = { url: rulesURL, auth: { bearer: token }, json: true };
		try { 
			const response = await get(requestConfig);
			if (response.statusCode === 200) {
				this.log('twit get rules', 'getted', response.body.data, 'success');
			}else if (response.statusCode === 403) {
				this.log('twit get rules', '403', response.body);
			} else {
				this.log('twit get rules', '!200', response); //response.body.error.message
			}
		} catch (err) {
			this.log('twit get rules', 'getting', err);
		}
	}
	
	async deleteRule(rule){
		const requestConfig = { url: rulesURL, auth: { bearer: token }, json: rule };
		try {
			const response = await post(requestConfig);
			if (response.statusCode === 200 || response.statusCode === 201) {
				this.log('twit delete rules', 'deleted', response.body, 'success');
			} else {
				this.log('twit delete rules', '!200', response);
			}
		} catch (err) {
			this.log('twit delete rules', 'deleting', err);
		}
	}
	
	checkHeartbeat () {
		if(this.lastHeartbeat && moment().diff(this.lastHeartbeat, 'minute') > 3 ){
			this.hadHeartbeatError = true;
			this.updateState('twit heartbeat', 'not receiving', this.lastHeartbeat.fromNow());
			this.lastHeartbeat = null;
		}
		if(this.hadHeartbeatError && this.lastHeartbeat){
			this.hadHeartbeatError = false;
			this.myGuild.channelLog.send('hearbeat is back');
		}
	}
	
	doAction(message, isAdmin){
		if (message.content === '!twit help') {
			message.reply(this.help);
		}
		if (message.content === '!twit get') {
			this.getRules();
		}
		if (message.content === '!twit check') {
			this.timeout = 0;
			message.reply(this.lastHeartbeat ? this.lastHeartbeat.fromNow() : this.hadHeartbeatError ? 'error' : '?');
		}
		if(!isAdmin){return;}
		if (message.content === '!twit start') {
			this.start();
		}
		if (message.content === '!twit stop') {
			this.stop();
		}
		if (message.content.startsWith('!twit add ')) {
			let args = message.content.split(' ');
			this.addRules({ 'add': [{ 'value' : 'from:' + args[2], 'tag' : args[3] }] });
		}
		if (message.content.startsWith('!twit del ')) {
			let args = message.content.split(' ');
			this.deleteRule({ 'delete': { 'ids': [args[2]] } });
		}
	}
	
	get help(){
		return `TWIT HELP : 
\`\`\`
!twit get       : (everyone)      show the current used rules.
!twit check     : (everyone)      check if there is still a heartbeat.
!twit start     : (admin)         start the stream, the heartbeat and his check every minute.
!twit stop      : (admin)         abort the stream.
!twit add El_Lemming tag_exemple       : (admin)         add a rule for follow El_Lemming and tagging it with tag_exemple.
!twit del 0000000000000000000          : (admin)         delete a rule by is id.
\`\`\`
`;
	}
	
	async updateState(source, title, details, type){
		const embed = new Discord.MessageEmbed()
		.setColor(type === 'info' ? 'BLUE': type === 'success' ? 'GREEN' : 'RED')
		.setTitle(source)
		.setDescription(title)
		.setTimestamp();
		this.state.edit(embed);
		if(!type){
			this.log(source, title, details);
		}
	}
	
};

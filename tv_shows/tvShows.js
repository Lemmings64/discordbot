const Discord = require('discord.js');
const Parser = require('rss-parser');
const cron = require('node-cron');
const fs = require('fs');
const seek = require('./seek.json');
//npm install discord.js rss-parser node-cron
const FEED = 'https://www.xxxxxxx-xxxx.xxxxx/rss.xml';

module.exports = class TvShows {
	constructor(discord, myGuild, sendMsgAsync, log) {
		this.discord = discord;
		this.myGuild = myGuild;
		this.channelLog = this.myGuild.channelLog;
		this.sendMsgAsync = sendMsgAsync;
		this.log = log;
		this.parser = new Parser();
		this.titles = [];
		this.seek = seek;
		this.seekCron = cron.schedule('0 * * * *', () => this.seekRss(), {scheduled: false});
		this.state = null;
	}
	
	async startSeek(){
		const embed = new Discord.MessageEmbed()
			.setColor('GREEN')
			.setTitle('TV')
			.setDescription('started')
			.setTimestamp()
		if(this.state){
			await this.state.edit(embed);
		}else{
			await this.myGuild.channelStates.send(embed).then(msg => {this.state = msg});
		}
		this.seekRss();
		this.seekCron.start();
	}
	
	stop() {
		this.seekCron.destroy();
		this.myGuild.channelStates.bulkDelete([this.state]);
		this.state = null;
	}
	
	seekRss(){
		this.parser.parseURL(FEED).then(feed => {
			feed.items.forEach(item => {
				let title = item.title;
				if(!this.titles.includes(title) && this.seek.includes(title))
				{
					this.titles.push(title);
					setTimeout(()=>this.titles.shift(),259200000);
					let subTitle = `\n ${item.content.substring(0, item.content.indexOf('\r\n'))}`;
					let url = `\n <${item.link}>`;
					this.sendMsgAsync(this.myGuild.channelSeek, title + subTitle + url);
				}
			});
			this.updateState('tv seek rss', 'showing movies', null, 'info'); 
		}).catch(err => this.updateState('tv seek rss', 'getting', err));
	}
	
	getSeek(message){
		message.reply(this.seekTxt);
	}
	
	addSeek(tvShow){
		this.seek.push(tvShow);
		this.saveFile();
	}
	
	removeSeek(tvShow){
		this.seek.splice(this.seek.indexOf(tvShow), 1);
		this.saveFile();
	}
	
	saveFile(){
		fs.writeFile(__dirname+'/seek.json', this.seekTxt, (err) => {
			if (err) this.log('tv save file', 'saving', err); 
		});
	}
	
	doAction(message, isAdmin){
		if (message.content === '!tv help') {
			message.reply(this.help);
		}
		if(!isAdmin){return;}
		if (message.content === '!tv start') {
			this.startSeek();
		}
		if (message.content === '!tv stop') {
			this.stop();
		}
		if (message.content === '!tv get') {
			this.getSeek(message);
		}
		if (message.content.startsWith('!tv add ')) {
			var tvShow = message.content.replace('!tv add ','');
			this.addSeek(tvShow);
		}
		if (message.content.startsWith('!tv del ')) {
			var tvShow = message.content.replace('!tv del ','');
			this.removeSeek(tvShow);
		}
	}
	
	get seekTxt(){
		return JSON.stringify(this.seek, null, 4);
	}
	get help(){
		return `TV HELP : 
\`\`\`
!tv start       : (admin)         start the seek tv shows rss loop.
!tv stop        : (admin)         stop the seeking.
!tv get         : (admin)         show the current watched tv shows.
!tv add Les Simpson - Saison 31 VOSTFR      : (admin)         add a tv show to watch on.
!tv del Les Simpson - Saison 31 VOSTFR      : (admin)         delete a tv show to watch on.
\`\`\`
`;
	}
	
	async updateState(source, title, details, type){
		const embed = new Discord.MessageEmbed()
		.setColor(type === 'info' ? 'BLUE': type === 'success' ? 'GREEN' : 'RED')
		.setTitle(source)
		.setDescription(title)
		.setTimestamp();
		this.state.edit(embed);
		if(!type){
			this.log(source, title, details);
		}
	}
};

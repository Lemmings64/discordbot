const Discord = require('discord.js');
const moment = require('moment');
const express = require('express');
const bodyParser = require('body-parser');
const util = require('util');
const request = require('request');
const path = require('path');
const socketIo = require('socket.io');
const http = require('http');
const https = require('https');
const _ = require('lodash');
const cron = require('node-cron');
const post = util.promisify(request.post);
const get = util.promisify(request.get);
const pokedex = require('./pokedex.json');
var gyms = require('./gyms.json').map(g=>g.locate);
//npm install discord.js moment express body-parser util request path socket.io http https lodash node-cron

module.exports = class twit {
	constructor(discord, myGuild, send, sendAsync, log) {
		this.discord = discord;
		this.myGuild = myGuild;
		this.channelLog = this.myGuild.channelLog;
		this.send = send;
		this.sendAsync = sendAsync;
		this.log = log;
		this.posted = [];
		this.getRaidsIntervalId = null;
		this.state = null;
	}
	
	async start(){
		const embed = new Discord.MessageEmbed()
			.setColor('GREEN')
			.setTitle('POGO')
			.setDescription('started')
			.setTimestamp();
		if(this.state){
			await this.state.edit(embed);
		}else{
			await this.myGuild.channelStates.send(embed).then(msg => {this.state = msg});
		}
		this.getRaids();
		if(!this.getRaidsIntervalId) {
			this.getRaidsIntervalId = setInterval(()=>this.getRaids(),300000);
		}
	}
	
	stop() {
		if(this.getRaidsIntervalId) {
			clearInterval(this.getRaidsIntervalId);
			this.getRaidsIntervalId = null;
		}
		this.myGuild.channelStates.bulkDelete([this.state]);
		this.state = null;
	}
	
	async getRaids () {
		https.get('https://xxxxxxxxxxxx.fr/getraids.php', (res) => {
			const { statusCode } = res;
			const contentType = res.headers['content-type'];
			res.setEncoding('utf8');
			let rawData = '';
			res.on('data', (chunk) => { rawData += chunk; });
			res.on('end', () => {
				let data = [];
				try {
					data = JSON.parse(rawData);
				} catch (err) {
					this.updateState('pogo getRaids', 'error parsing', rawData); 
				}
				
				for (var i = 0; i < data.length; i++) {
					var raid = data[i];
					var name = raid.gym_name;
					var start = raid.start_time.substring(11,16);
					var end = raid.end_time.substring(11,16);
					//&& raid.raid_level==='5'
					if(gyms.includes(raid.lat+raid.lon) && raid.pokemon==='9995' && !this.posted.includes(raid.id))
					{
						let msg = `${start} -> ${end} :     **${name}**`;
						let acronym = name.split(/[\s-', ]+/).reduce((r,w)=> r+=w.slice(0,1),'');
						let pok = raid.pokemon==='9995' ? '5' : pokedex[(raid.pokemon*1)-1].name.french;
						msg += `   (!r ${pok} ${_.toLower(acronym)} ${start})`;
						this.sendAsync(this.myGuild.channelRaids, msg); 
						this.posted.push(raid.id);
						setTimeout(()=>this.posted.shift(),7200000);
					}
				}
				this.updateState('pogo getRaids', 'showing raids', null, 'info'); 
			});
		}).on('error', err => {this.updateState('pogo getRaids', 'error fetching', err); });
	}
	
	caldis([lat1, long1, lat2, long2], message) {
		let r = Math.PI/180, c = Math.cos, s = Math.sin, time = 0; lat1 *= r; long1 *= r; lat2 *= r; long2 *= r;
		var coco = (c(lat1)*c(lat2)*c(long1)*c(long2)+c(lat1)*s(long1)*c(lat2)*s(long2)+s(lat1)*s(lat2)); 
		var km = Math.round(6371*Math.acos(coco)*1000)/1000;
		if(km>1500){time = 120;}
		else if(km>99){time =  Math.floor((85/1400)*km+29.9);}
		else if(km>22){time =  Math.floor((15/60)*km+10);}
		else {time =  Math.round((20/30)*km);}
		message.reply('km : ' + km + ', time : ' + time);
	}

	doAction(message, isAdmin){
		if (message.content === '!pogo help') {
			message.reply(this.help);
		}
		if (message.content.startsWith('!pogo calc ')) {
			let [,,...args] = message.content.split(/[, ]/);
			this.caldis(args, message);
		}
		if(!isAdmin){return;}
		if (message.content === '!pogo start') {
			this.start();
		}
		if (message.content === '!pogo stop') {
			this.stop();
		}
	}
	
	get help(){
		return `POGO HELP : 
\`\`\`
!pogo calc 48.890683,2.238642 43.1964175,6.0103415    : (everyone)      calcul the cooldown between to gps coords
!pogo start     : (admin)         start to report raids
!pogo stop      : (admin)         stop to report raids
\`\`\`
`;
	}
	
	async updateState(source, title, details, type){
		const embed = new Discord.MessageEmbed()
		.setColor(type === 'info' ? 'BLUE': type === 'success' ? 'GREEN' : 'RED')
		.setTitle(source)
		.setDescription(title)
		.setTimestamp();
		this.state.edit(embed);
		if(!type){
			this.log(source, title, details);
		}
	}
	
};
